variable "ami_sygnal" {
  type = string
}

variable "instance_type_sygnal" {
  type = string
}

variable "sygnal_ios" {
  type = list(string)
}

variable "sygnal_android" {
  type = list(string)
}

variable "sygnal_metrics_port" {
  type        = string
  description = "The port which sygnal exposes metrics on"
  default     = "9142"
}
