output "synapse_id" {
  value = aws_instance.synapse.id
}
output "ma1sd_id" {
  value = aws_instance.ma1sd.id
}
output "sygnal_id" {
  value = aws_instance.sygnal.id
}

output "synapse_ip" {
  value = aws_instance.synapse.private_ip
}

output "sygnal_ip" {
  value = aws_instance.sygnal.private_ip
}

output "ma1sd_ip" {
  value = aws_instance.ma1sd.private_ip
}
output "alb_id" {
  value = aws_lb.application.id
}

output "alb_dns_name" {
  value = aws_lb.application.dns_name
}

output "acm_certificate_arn" {
  value = aws_acm_certificate.alb_cert.arn
}
