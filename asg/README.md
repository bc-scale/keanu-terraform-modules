## ASG (Autoscaling Group)

The purpose of this module is to provide a Launch Configuration and Autoscaling
Group as a pair.

The module supports:

* spanning N Availability Zones
* load balancers may be associated with the ASG
* the health checks are not yet parameterized, (easy to change)
* the Launch Configuration supports an arbitrary list of security groups
* `lifecycle` and `create_before_destroy` are used to ensure updates are graceful
* public IPs may be enabled/disabled
* supports appending `extra_tags`
* all important details (instance type, ami, key, user data, iam profile) are
  specified as variables in the modules.

Note that, Terraform does not run a rolling update when an ASG/LC pair have
changed. After the ASG/LC have been updated, the EC2 instances running before
the update will still be running. As a result, you will need to terminate each
of those instances to perform a rolling update.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| ami | The base AMI for each AWS instance created | string | n/a | yes |
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| azs | list of availability zones to associate with the ASG | list | n/a | yes |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| elb\_names | list of load balancers to associate with the ASG (by name) | list | `<list>` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| iam\_profile | The IAM profile to associate with AWS instances in the ASG | string | `""` | no |
| instance\_type | The type of AWS instance (size) | string | `"t2.micro"` | no |
| key\_name | The name of the (AWS) SSH key to associate with the instance | string | n/a | yes |
| max\_nodes | The maximum number of nodes in each group | string | n/a | yes |
| min\_nodes | The minimum number of nodes in each group | string | n/a | yes |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| placement\_group | The `id` of the `aws_placement_group` to associate with the ASG | string | `""` | no |
| public\_ip | Boolean flag to enable/disable `map_public_ip_on_launch` in each `aws_subnet` | string | `"true"` | no |
| root\_volume\_size | The size of the EBS volume (in GB) for the root block device | string | `"15"` | no |
| root\_volume\_type | The type of EBS volume to use for the root block device | string | `"gp2"` | no |
| security\_group\_ids | list of security groups to associate with the ASG (by id) | list | n/a | yes |
| subnet\_ids | list of subnets to associate with the ASG (by id) | list | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |
| termination\_policies | A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are OldestInstance, NewestInstance, OldestLaunchConfiguration, ClosestToNextInstanceHour, Default. | list | `<list>` | no |
| user\_data | The user_data string to pass to cloud-init | string | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| id | ID of the ASG |
| lc\_id | ID of the Launch Config |
| lc\_name | Name of the Launch Config |
| name | Name of the ASG |

