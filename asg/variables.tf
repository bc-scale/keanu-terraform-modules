variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = "string"
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = "string"
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = "string"
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "key_name" {
  description = "The name of the (AWS) SSH key to associate with the instance"
  type        = "string"
}

variable "ami" {
  description = "The base AMI for each AWS instance created"
  type        = "string"
}

variable "iam_profile" {
  default     = ""
  description = "The IAM profile to associate with AWS instances in the ASG"
  type        = "string"
}

variable "instance_type" {
  default     = "t2.micro"
  description = "The type of AWS instance (size)"
  type        = "string"
}

variable "placement_group" {
  default     = ""
  description = "The `id` of the `aws_placement_group` to associate with the ASG"
  type        = "string"
}

variable "user_data" {
  default     = ""
  description = "The user_data string to pass to cloud-init"
  type        = "string"
}

variable "max_nodes" {
  description = "The maximum number of nodes in each group"
  type        = "string"
}

variable "min_nodes" {
  description = "The minimum number of nodes in each group"
  type        = "string"
}

variable "public_ip" {
  default     = "true"
  description = "Boolean flag to enable/disable `map_public_ip_on_launch` in each `aws_subnet`"
  type        = "string"
}

variable "azs" {
  description = "list of availability zones to associate with the ASG"
  type        = "list"
}

variable "subnet_ids" {
  description = "list of subnets to associate with the ASG (by id)"
  type        = "list"
}

variable "security_group_ids" {
  description = "list of security groups to associate with the ASG (by id)"
  type        = "list"
}

variable "elb_names" {
  default     = []
  description = "list of load balancers to associate with the ASG (by name)"
  type        = "list"
}

variable "root_volume_type" {
  default     = "gp2"
  description = "The type of EBS volume to use for the root block device"
  type        = "string"
}

variable "root_volume_size" {
  default     = "15"
  description = "The size of the EBS volume (in GB) for the root block device"
  type        = "string"
}

variable "termination_policies" {
  default     = []
  description = "A list of policies to decide how the instances in the auto scale group should be terminated. The allowed values are OldestInstance, NewestInstance, OldestLaunchConfiguration, ClosestToNextInstanceHour, Default."
  type        = "list"
}
