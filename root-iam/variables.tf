variable "root_account_admin_user_names" {
  type        = list(string)
  description = "IAM user names to grant admin access to Root account"
  default     = []
}

variable "root_account_readonly_user_names" {
  type        = list(string)
  description = "IAM user names to grant readonly access to Root account"
  default     = []
}

variable "root_account_namespace" {
  type = string
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
  default     = "root"
}

variable "admin_group_name" {
  type        = string
  description = "The name of the group that gives admin permissions"
  default     = "admin"
}

variable "billing_group_name" {
  type        = string
  description = "The name of the group that controls access to the billing"
  default     = "billing"
}
