## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| admin\_group\_name | The name of the group that gives admin permissions | string | `"admin"` | no |
| billing\_group\_name | The name of the group that controls access to the billing | string | `"billing"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | `"root"` | no |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| root\_account\_admin\_user\_names | IAM user names to grant admin access to Root account | list | `<list>` | no |
| root\_account\_namespace |  | string | n/a | yes |
| root\_account\_readonly\_user\_names | IAM user names to grant readonly access to Root account | list | `<list>` | no |

## Outputs

| Name | Description |
|------|-------------|
| admin\_group |  |
| admin\_switchrole\_url | URL to the IAM console to switch to the admin role |
| billing\_full\_access\_group |  |
| readonly\_group |  |
| readonly\_switchrole\_url | URL to the IAM console to switch to the readonly role |
| role\_admin\_arn | Admin role ARN |
| role\_readonly\_arn | Readonly role ARN |

