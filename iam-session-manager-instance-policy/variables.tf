variable "region" {
  type        = "string"
  description = "AWS Region this session manager config is for"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = "string"
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = "string"
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = "string"
  description = "Name  (e.g. `app` or `cluster`)"
}

variable "s3_bucket_name" {
  type        = "string"
  description = "The name of bucket to store session logs. Specifying this enables writing session output to an Amazon S3 bucket."
}

variable "s3_key_prefix" {
  type        = "string"
  description = "To write output to a sub-folder, enter a sub-folder name."
}
