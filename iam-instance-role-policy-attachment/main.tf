module "label" {
  source      = "../null-label"
  namespace   = "${var.namespace}"
  name        = "${var.name}"
  environment = "${var.environment}"
  delimiter   = "${var.delimiter}"
  attributes  = "${var.attributes}"
  tags        = "${var.tags}"
}

data "aws_iam_policy_document" "instance_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "instance" {
  name               = "${module.label.id}"
  path               = "/"
  assume_role_policy = "${data.aws_iam_policy_document.instance_assume_role.json}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_role_policy_attachment" "default" {
  role = "${aws_iam_role.instance.id}"

  // TODO(tf12) remove extra count var, rely on computed count
  #count      = "${length(var.iam_policy_arns)}"
  count = "${var.iam_policy_arn_count}"

  policy_arn = "${var.iam_policy_arns[count.index]}"
}
