variable "vpc_cidr_block" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "database_name" {
  type = string
}

variable "database_username" {
  type = string
}

variable "database_password" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "engine" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "allocated_storage" {
  ## TODO(tf12) string to int
  type = string
}

variable "family" {
  type = string
}

variable "major_engine_version" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "deletion_protection_enabled" {
  ## TODO(tf12) string to boolean
  type = string
}

variable "skip_final_snapshot" {
  ## TODO(tf12) string to boolean
  type = string
}

variable "allow_major_version_upgrade" {
  ## TODO(tf12) string to boolean
  type = string
}

variable "apply_immediately" {
  ## TODO(tf12) string to boolean
  type = string
}

variable "backup_retention_period" {
  ## TODO(tf12) string to int
  type = string
}

### label
variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `namespace`, `environment`, `name`, and `attributes`"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)"
}

variable "namespace" {
  type        = string
  description = "Namespace (e.g. `keanu`)"
}

variable "environment" {
  type        = string
  description = "environment (e.g. `prod`, `dev`, `staging`, `infra`)"
}

variable "name" {
  type        = string
  description = "Name  (e.g. `app` or `cluster`)"
}
