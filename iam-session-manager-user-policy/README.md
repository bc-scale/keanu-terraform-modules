## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| kms\_key\_arn | The ARN of a CMK; enables the creation of a data encryption key that will be used to encrypt session data | string | n/a | yes |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| region | AWS Region this session manager config is for | string | n/a | yes |
| tag\_key | The tag key on EC2 instances that users will be able to access via session manager | string | n/a | yes |
| tag\_value | The tag value on EC2 instances that users will be able to access via session manager | string | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| user\_session\_manager\_full\_admin\_policy\_arn | ARN of the created policy that allows users full access to all session manager sessions |
| user\_session\_manager\_tagged\_instances\_policy\_arn | ARN of the created policy that allows users access to manage their sessions on instances tagged with ${var.tag_key}=${var.tag_value} |

