terraform {
  required_version = ">= 0.11"

  required_providers {
    aws = ">= 1.36.0"
  }
}

module "label" {
  source      = "../null-label"
  namespace   = "${var.namespace}"
  environment = "${var.environment}"
  name        = "${var.name}"
  delimiter   = "${var.delimiter}"
  attributes  = "${var.attributes}"
  tags        = "${var.tags}"
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "user_session_manager_full_admin" {
  statement = {
    actions = [
      "ssm:StartSession",
      "ssm:TerminateSession",
      "ssm:ResumeSession",
      "ssm:DescribeSessions",
      "ssm:GetConnectionStatus",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "user_session_manager_full_admin" {
  name        = "user-session-manager-full-admin-${module.label.id}"
  description = "Policy that allows users to manage all session manager sessions"
  policy      = "${data.aws_iam_policy_document.user_session_manager_full_admin.json}"
}

data "aws_iam_policy_document" "user_session_manager_tagged_instances" {
  statement = {
    actions = ["ssm:StartSession"]

    resources = [
      "arn:aws:ec2:*:*:instance/*",
    ]

    condition {
      test     = "StringLike"
      variable = "ssm:resourceTag/${var.tag_key}"

      values = [
        "${var.tag_value}",
      ]
    }
  }

  statement = {
    actions = [
      "ssm:DescribeSessions",
      "ssm:GetConnectionStatus",
      "ssm:DescribeInstanceProperties",
      "ec2:DescribeInstances",
    ]

    resources = [
      "*",
    ]
  }

  statement = {
    actions = [
      "ssm:CreateDocument",
      "ssm:UpdateDocument",
      "ssm:GetDocument",
    ]

    resources = [
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:document/SSM-SessionManagerRunShell",
    ]
  }

  statement = {
    actions = [
      "ssm:TerminateSession",
    ]

    resources = [
      "arn:aws:ssm:*:*:session/&{aws:username}-*",
    ]
  }

  statement = {
    actions = [
      "kms:GenerateDataKey",
    ]

    resources = [
      "${var.kms_key_arn}",
    ]
  }
}

resource "aws_iam_policy" "user_session_manager_tagged_instances" {
  name        = "user-session-manager-tagged-${module.label.id}"
  description = "Policy that allows users to create sessions on instances tagged with ${var.tag_key}=${var.tag_value}"
  policy      = "${data.aws_iam_policy_document.user_session_manager_full_admin.json}"
}
