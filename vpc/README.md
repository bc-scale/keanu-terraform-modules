## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| availability\_zone\_1 |  | string | n/a | yes |
| availability\_zone\_2 |  | string | n/a | yes |
| cidr\_block |  | string | `"10.55.0.0/16"` | no |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| region |  | string | n/a | yes |
| subnet\_private\_1 |  | string | `"10.55.2.0/24"` | no |
| subnet\_private\_2 |  | string | `"10.55.3.0/24"` | no |
| subnet\_public\_1 |  | string | `"10.55.0.0/24"` | no |
| subnet\_public\_2 |  | string | `"10.55.1.0/24"` | no |
| subnet\_rds\_a\_1 |  | string | `"10.55.4.0/28"` | no |
| subnet\_rds\_a\_2 |  | string | `"10.55.4.16/28"` | no |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| igw\_id | The ID of the Internet Gateway |
| ipv6\_cidr\_block | The IPv6 CIDR block |
| nat\_gw\_eip\_id |  |
| nat\_gw\_eip\_public\_ip |  |
| nat\_gw\_id |  |
| region |  |
| sg\_private\_ssh\_id |  |
| subnet\_private\_1\_availability\_zone |  |
| subnet\_private\_1\_cidr\_block |  |
| subnet\_private\_1\_id |  |
| subnet\_public\_1\_availability\_zone |  |
| subnet\_public\_1\_cidr\_block |  |
| subnet\_public\_1\_id |  |
| subnet\_public\_2\_availability\_zone |  |
| subnet\_public\_2\_cidr\_block |  |
| subnet\_public\_2\_id |  |
| subnet\_rds\_a\_1\_availability\_zone |  |
| subnet\_rds\_a\_1\_cidr\_block |  |
| subnet\_rds\_a\_1\_id |  |
| subnet\_rds\_a\_2\_availability\_zone |  |
| subnet\_rds\_a\_2\_cidr\_block |  |
| subnet\_rds\_a\_2\_id |  |
| subnet\_rds\_subnet\_group |  |
| vpc\_cidr\_block | The CIDR block of the VPC |
| vpc\_default\_network\_acl\_id | The ID of the network ACL created by default on VPC creation |
| vpc\_default\_route\_table\_id | The ID of the route table created by default on VPC creation |
| vpc\_default\_security\_group\_id | The ID of the security group created by default on VPC creation |
| vpc\_id | The ID of the VPC |
| vpc\_ipv6\_association\_id | The association ID for the IPv6 CIDR block |
| vpc\_main\_route\_table\_id | The ID of the main route table associated with this VPC. |

