## Persistent Data on EBS

This module provides an EBS volume and associated IAM profile/role to be
used with an EC2 instance or auto-scaling group. This module is best when
used in conjunction with a single-node auto-scaling group, and with the
init-snippet that attaches the named EBS volume on boot.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| az | The AWS Availability Zone (AZ) to create the instance in | string | n/a | yes |
| delimiter | Delimiter to be used between `namespace`, `environment`, `name`, and `attributes` | string | `"-"` | no |
| encrypted | Boolean, whether or not to encrypt the EBS block device | string | `"true"` | no |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| iops | The amount of IOPS to provision for the EBS block device | string | `""` | no |
| kms\_key\_id | ID of the KMS key to use when encyprting the EBS block device | string | `""` | no |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| region | The AWS region to deploy to | string | `""` | no |
| size | Size (in GB) of EBS volume to use for the EBS block device | string | `"15"` | no |
| snapshot\_id | The ID of the snapshot to base the EBS block device on | string | `""` | no |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |
| volume\_type | Type of EBS volume to use for the EBS block device | string | `"gp2"` | no |

## Outputs

| Name | Description |
|------|-------------|
| iam\_profile\_arn | `arn` exported from the `aws_iam_instance_profile` |
| iam\_profile\_id | `id` exported from the `aws_iam_instance_profile` |
| iam\_profile\_policy\_document | `policy` exported from the `aws_iam_role_policy` |
| iam\_role\_arn | `arn` exported from the `aws_iam_role` |
| iam\_role\_name | `name` exported from the `aws_iam_role` |
| volume\_id | `id` exported from the `aws_ebs_volume` |

