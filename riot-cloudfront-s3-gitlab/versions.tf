terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.0"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
    }
    gitlab = {
      source = "terraform-providers/gitlab"
    }
    template = {
      source = "hashicorp/template"
    }
    archive = {
      source = "hashicorp/archive"
    }
  }
  required_version = ">= 0.13"
}
