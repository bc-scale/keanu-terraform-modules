data "aws_region" "current" {}
module "label_distribution" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = concat(var.attributes, ["distribution"])
  tags        = var.tags
}

module "label_origin" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = concat(var.attributes, ["origin"])
  tags        = var.tags
}

module "label_deploy" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.19.2"
  namespace   = var.namespace
  name        = var.name
  environment = var.environment
  delimiter   = var.delimiter
  attributes  = concat(var.attributes, ["deployer"])
  tags        = var.tags
}



resource "aws_cloudfront_origin_access_identity" "origin" {
  comment = module.label_distribution.id
}


data "aws_iam_policy_document" "origin" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::$${bucket_name}$${origin_path}*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["arn:aws:s3:::$${bucket_name}"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin.iam_arn]
    }
  }
}

data "template_file" "default" {
  template = data.aws_iam_policy_document.origin.json

  vars = {
    origin_path = coalesce(var.origin_path, "/")
    bucket_name = aws_s3_bucket.origin.id
  }
}

resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.origin.id
  policy = data.template_file.default.rendered
}

resource "aws_s3_bucket" "origin" {
  bucket = module.label_origin.id
  acl    = "private"

  website {
    index_document = "index.html"
  }

  tags = module.label_origin.tags
}


resource "aws_iam_user" "deploy_user" {
  name = module.label_deploy.id
  tags = module.label_deploy.tags
}

resource "aws_iam_access_key" "deploy_user_key_v3" {
  user = aws_iam_user.deploy_user.name
}

resource "aws_iam_user_policy" "deploy_rw" {
  name = "AllowDeployer${var.namespace}${var.environment}${var.name}"
  user = aws_iam_user.deploy_user.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "AllowDeployFor${var.namespace}${var.environment}${var.name}",
        "Effect": "Allow",
        "Action": [
          "s3:GetObject",
          "s3:PutObject",
          "s3:DeleteObject",
          "s3:PutObjectAcl"
        ],
        "Resource": "${aws_s3_bucket.origin.arn}/*"
      }
    ]
}
EOF

}

resource "gitlab_project_variable" "aws_region" {
  project           = var.gitlab_project
  key               = var.gitlab_var_name_aws_default_region
  value             = aws_s3_bucket.origin.region
  environment_scope = "*"
}

resource "gitlab_project_variable" "bucket_name" {
  project           = var.gitlab_project
  key               = var.gitlab_var_name_bucket_name
  value             = aws_s3_bucket.origin.id
  environment_scope = "*"
}

resource "gitlab_project_variable" "aws_access_key_id" {
  project           = var.gitlab_project
  key               = var.gitlab_var_name_aws_access_key_id
  value             = aws_iam_access_key.deploy_user_key_v3.id
  protected         = false
  environment_scope = "*"
  masked            = true
}

resource "gitlab_project_variable" "aws_secret_access_key" {
  project           = var.gitlab_project
  key               = var.gitlab_var_name_aws_secret_access_key
  value             = aws_iam_access_key.deploy_user_key_v3.secret
  protected         = false
  environment_scope = "*"
  masked            = true
}

resource "aws_s3_bucket_object" "config_json" {
  bucket  = aws_s3_bucket.origin.id
  key     = "config.json"
  content = var.riot_config
  acl     = "public-read"
}

resource "aws_cloudfront_distribution" "site" {
  depends_on = [
    aws_s3_bucket.origin,
  ]
  default_root_object = "index.html"

  origin {
    origin_id = aws_s3_bucket.origin.id

    #origin_id   = "CustomOriginConfig"
    domain_name = aws_s3_bucket.origin.bucket_regional_domain_name
    origin_path = var.origin_path
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin.cloudfront_access_identity_path
    }
  }

  enabled             = true
  price_class         = "PriceClass_All"
  comment             = module.label_distribution.id
  aliases             = [var.domain_name]
  is_ipv6_enabled     = true
  wait_for_deployment = false

  default_cache_behavior {
    allowed_methods = ["HEAD", "GET", "OPTIONS"]
    cached_methods  = ["HEAD", "GET"]

    target_origin_id = aws_s3_bucket.origin.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    compress               = true
    min_ttl                = 60
    default_ttl            = 7200
    max_ttl                = 86400

    lambda_function_association {
      event_type   = "origin-request"
      lambda_arn   = aws_lambda_function.folder_index_redirect.qualified_arn
      include_body = false
    }
  }

  ordered_cache_behavior {
    path_pattern = "config.json"

    allowed_methods  = ["HEAD", "GET", "OPTIONS"]
    cached_methods   = ["HEAD", "GET"]
    target_origin_id = aws_s3_bucket.origin.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    iam_certificate_id       = var.iam_certificate_id
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method       = "sni-only"
  }
  tags = module.label_distribution.tags
}

resource "cloudflare_record" "site_domain" {
  zone_id = var.cloudflare_zone_id
  name    = var.domain_name
  value   = aws_cloudfront_distribution.site.domain_name
  type    = "CNAME"
}
