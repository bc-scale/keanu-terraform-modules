## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attributes | Additional attributes (e.g. `1`) | list | `<list>` | no |
| cloudflare\_zone |  | string | n/a | yes |
| delimiter | Delimiter to be used between `namespace`, `stage`, `name` and `attributes` | string | `"-"` | no |
| domain\_name | The domain name that the distribution will be served under (e.g., web.keanu.im) | string | n/a | yes |
| environment | environment (e.g. `prod`, `dev`, `staging`, `infra`) | string | n/a | yes |
| gitlab\_project | The gitlab project that will deploy the app | string | n/a | yes |
| homeserver\_url | The full URL to the default homeserver | string | n/a | yes |
| iam\_certificate\_id | The IAM certificate identifier for serving the cloudfront distribution | string | n/a | yes |
| identity\_server\_url | The full URL to the default identity server | string | n/a | yes |
| name | Name  (e.g. `app` or `cluster`) | string | n/a | yes |
| namespace | Namespace (e.g. `keanu`) | string | n/a | yes |
| origin\_path | An optional element that causes CloudFront to request your content from a directory in your Amazon S3 bucket or your custom origin. It must begin with a /. Do not add a / at the end of the path. | string | `""` | no |
| riot\_config | The JSON config for riot | string | n/a | yes |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`) | map | `<map>` | no |

## Outputs

| Name | Description |
|------|-------------|
| bucket\_id |  |
| cloudfront\_distribution\_id |  |
| deployer\_aws\_access\_key\_id |  |
| deployer\_aws\_secret\_access\_key |  |
| deployer\_username |  |
| gitlab\_project |  |

