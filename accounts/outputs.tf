output "account_arn" {
  value = var.account_arn
}

output "account_id" {
  value = var.account_id
}

output "organization_account_access_role" {
  value = var.organization_account_access_role
}

output "switchrole_url" {
  description = "URL to the IAM console to switch to the $${var.environment} account organization access role"
  value       = module.admin_organization_access_group.switchrole_url
}
